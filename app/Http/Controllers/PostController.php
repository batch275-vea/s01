<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// to have access with the authenticated user
use Illuminate\Support\Facades\Auth;
// to have access with the queries related to the post entity/model.
use App\Models\Post;

class PostController extends Controller
{
    //action to return a view containing a form for post creation
    public function create(){
        return view('posts.create');
    }

    //action to receive the form data and subsequently store said data in the post table
    public function store(Request $request){
        // if there is an authenticated user
        if(Auth::user()){
            //instantiate a new post object from the Post model
            $post = new Post;
            // define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            //get the id of the authenticated user and set it as foreign key (user_id)
            $post->user_id = (Auth::user()->id);
            //save this post object in the database 
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    //action taht will return a view showing all blog posts
    public function index(){
        /*This is same with */
        //DB::table('Post')->get();
        $posts = Post::get();
        //The "with()" method will alow us to pass information from the controller to view page
        return view('posts.index')->with('posts', $posts);
    }

    //Activity 2
    public function welcome(){
        $posts = Post::inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    //action to show only the posts authored by the authenticated user.
    public function myPost(){
        if(Auth::user()){
            //we are able to fetch the posts related to a specific user because of the establish relationship between the models.
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    //action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown.
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }
}
